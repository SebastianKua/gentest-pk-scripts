# QMM-GENTEST GIT

This is a repository storing all the latest scripts and hardware files necessary to run GENTEST

**Please edit .gitignore file to remove all "#" from the files at the bottom to prevent edits made to the *./Hardware* folder from being overwritten.**

## Steps
 - Pull from repository
 - Edit .gitignore to prevent tester specific *Hardware* fies
 - 